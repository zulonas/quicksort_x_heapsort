#include <iostream>
#include <stdlib.h>
#include <chrono>

#define C_MAXSIZE 128000

#include "binarytree.h"
#include "linkedlist.h"
#include "quicksort.h"
#include "car.h"

void generateCarList(Linkedlist<Car>* list, int size)
{
	for (int x = 0; x < size; x++) {
		Car temp;
		list->addLast(temp);
	}
}

/*
 * iterate through objects in range
 */
template <typename T>
int loopInRange(void (*f)(void *), T arr[], int arrSize, int start_n,
		int stop_n)
{
	if (start_n < 0 || start_n >= arrSize || start_n > stop_n
			|| stop_n >= arrSize) {
		return 1;
	}

	for (int x = start_n; x < stop_n; x++)
		(*f)(&(arr[x]));

	return 0;
}

/*
 * assume void* == car*
 */
void printCarData(void *car)
{
	Car* data = static_cast<Car*>(car);
	std::cout << *data << std::endl;
}

int main()
{
	Car array1[C_MAXSIZE];
	auto start1 = std::chrono::steady_clock::now();
	quicksort(array1, 0, C_MAXSIZE - 1);
	auto end1 = std::chrono::steady_clock::now();
	auto diff1 = end1 - start1;
	std::cout << std::chrono::duration <double, std::milli> (diff1).count()
		<< " ms" << std::endl;
	std::cout << "=================================" << std::endl;
	loopInRange(&printCarData, array1, C_MAXSIZE, 0, 10);
	std::cout << std::endl;

	//Linkedlist<Car> qwe;
	//generateCarList(&qwe, C_MAXSIZE);
	//auto start2 = std::chrono::steady_clock::now();
	//qwe.sort();
	//auto end2 = std::chrono::steady_clock::now();
	//auto diff2 = end2 - start2;
	//std::cout << std::chrono::duration <double, std::milli> (diff2).count()
		       //<< " ms" << std::endl;
	//std::cout << "=================================" << std::endl;
	//qwe.loopInRange(&printCarData, 0, 10);
	//std::cout << std::endl;

	//BinaryTree<Car> btree(C_MAXSIZE);
	//auto start3 = std::chrono::steady_clock::now();
	//btree.heap_sort();
	//auto end3 = std::chrono::steady_clock::now();
	//auto diff3 = end3 - start3;
	//std::cout << std::chrono::duration <double, std::milli> (diff3).count()
		       //<< " ms" << std::endl;
	//std::cout << "=================================" << std::endl;
	//btree.loopInRange(&printCarData, C_MAXSIZE - 10, C_MAXSIZE - 1);
	//std::cout << std::endl;

	return 0;
}
