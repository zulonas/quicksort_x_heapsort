#ifndef linkedlist_h
#define linkedlist_h

template <typename T>
struct Linkedlist_Node
{
	T data;
	Linkedlist_Node<T>* next;
	Linkedlist_Node<T>* prev;
};

template <typename T>
class Linkedlist
{
	public:
		Linkedlist();
		~Linkedlist();
		void addFirst(T data);
		void addLast(T data);
		T getFirst();
		void clean();
		void sort();
		void quicksort(Linkedlist_Node<T> *l, Linkedlist_Node<T> *h);
		Linkedlist_Node<T>* partition(Linkedlist_Node<T> *l,
				Linkedlist_Node<T> *h);
		void forEach(void (*f)(void *));
		void loopInRange(void (*f)(void *), int, int);
	private:
		Linkedlist_Node<T> *head;
		Linkedlist_Node<T> *tail;
		size_t size;
};

template <typename T>
Linkedlist<T>::Linkedlist()
{
	head = tail = nullptr;
	size = 0;
}

template <typename T>
Linkedlist<T>::~Linkedlist()
{
	this->clean(); /* it also performs delete head, tail */
	head = tail = nullptr;
	size = 0;
}

template <typename T>
void Linkedlist<T>::forEach(void (*f)(void *))
{
	Linkedlist_Node<T> *temp = head;

	while (temp) {
		(*f)(&(temp->data));
		temp = temp->next;
	}
}

template <typename T>
void Linkedlist<T>::addFirst(T data)
{
	Linkedlist_Node<T> *newNode = new Linkedlist_Node<T>;
	newNode->data = data;

	if (!head) {
		tail = newNode;
	} else {
		head->prev = newNode;
		newNode->next = head;
	}

	head = newNode;
	size++;
}

template <typename T>
void Linkedlist<T>::addLast(T data)
{
	Linkedlist_Node<T> *newNode = new Linkedlist_Node<T>;
	newNode->data = data;

	if (!tail) {
		head = newNode;
	} else {
		tail->next = newNode;
		newNode->prev = tail;
	}

	tail = newNode;
	size++;
}

template <typename T>
void Linkedlist<T>::clean()
{
	Linkedlist_Node<T> *temp = nullptr;

	while (head)
	{
		temp = head->next;
		delete head;
		head = temp;
	}

	temp = nullptr;
}


template <typename T>
T Linkedlist<T>::getFirst()
{
	return head->data;
}

template <typename T>
void Linkedlist<T>::sort()
{
	quicksort(head, tail);
}

/*
 * usage quicksort(head, tail);
 */
template <typename T>
void Linkedlist<T>::quicksort(Linkedlist_Node<T> *l, Linkedlist_Node<T> *h)
{
	if (h != NULL && l != h && l != h->next) {
		Linkedlist_Node<T> *part = partition(l, h);
		quicksort(l, part->prev);
		quicksort(part->next, h);
	}
}

template <typename T>
Linkedlist_Node<T>* Linkedlist<T>::partition(Linkedlist_Node<T> *l,
		Linkedlist_Node<T> *h)
{
	T pivot = h->data;
	Linkedlist_Node<T> *i = l->prev;

	for (Linkedlist_Node<T>* j = l; j != h; j = j->next) {
		if (!(j->data < pivot))
			continue;

		i = (!i) ? l : i->next;
		std::swap(i->data, j->data);
	}
	i = (!i) ? l : i->next;
	std::swap(i->data, h->data);

	return i;
}

template <typename T>
void Linkedlist<T>::loopInRange(void (*f)(void *), int start_i, int stop_i)
{
	Linkedlist_Node<T> *temp = head;

	for (int x = 0; x <= start_i; x++)
		temp = temp->next;

	for (int x = start_i; x <= stop_i; x++) {
		(*f)(&(temp->data));
		temp = temp->next;
	}
}
#endif
