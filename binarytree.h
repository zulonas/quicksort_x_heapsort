#ifndef binarytree_h
#define binarytree_h

#include <vector>

template <typename T>
class BinaryTree
{
	public:
		std::vector<T> heap;
		BinaryTree();
		BinaryTree(int);
		BinaryTree(T arr[], int size);
		void build_heap();
		void heapify(int, int);
		void heap_sort();
		int leftChild(int);
		int rightChild(int);
		int parent(int);
		void insert(T);
		void precUp(int);
		int loopInRange(void (*f)(void *), int, int);
};

template <typename T>
BinaryTree<T>::BinaryTree()
{
}

/*
 * Generate tree using default object data
 */
template <typename T>
BinaryTree<T>::BinaryTree(int size)
{
	for (int x = 0; x < size; x++) {
		T object;
		heap.push_back(object);
	}
	build_heap();
}

/*
 * Generate tree using predefined array
 */
template <typename T>
BinaryTree<T>::BinaryTree(T arr[], int size)
{
	for (int x = 0; x < size; x++)
		heap.push_back(arr[x]);
	build_heap();
}

template <typename T>
void BinaryTree<T>::heapify(int size, int index)
{
	int larg = index; //largest value;
	int l_child = leftChild(index);
	int r_child = rightChild(index);

	//check left child
	if (l_child < size && heap[l_child] > heap[larg])
		larg = l_child;

	//check right child
	if (r_child < size && heap[r_child] > heap[larg])
		larg = r_child;

	//swap and heapify subtree
	if (larg != index) {
		std::swap(heap[larg], heap[index]);
		heapify(size, larg);
	}
}

template <typename T>
void BinaryTree<T>::heap_sort()
{
	for (int x = heap.size() - 1; x > 0; x--) {
		std::swap(heap[0], heap[x]);
		heapify(x, 0);
	}
}

template <typename T>
void BinaryTree<T>::build_heap()
{
	for (int x = parent(heap.size()); x >= 0; x--)
		heapify(heap.size(), x);
}

template <typename T>
int BinaryTree<T>::leftChild(int index)
{
	return (2 * index) + 1;
}

template <typename T>
int BinaryTree<T>::rightChild(int index)
{
	return (2 * index) + 2;
}

template <typename T>
int BinaryTree<T>::parent(int index)
{
	return ((index - 1) / 2);
}

template <typename T>
void BinaryTree<T>::insert(T data)
{
	heap.push_back(data);
	precUp(heap.size() - 1);
}

template <typename T>
void BinaryTree<T>::precUp(int index)
{
	if (index == 0)
		return;

	if (heap[index] > heap[parent(index)]) {
		std::swap(heap[index], heap[parent(index)]);
		precUp(parent(index));
	}
}

// get callback pointer to specific object in range
template <typename T>
int BinaryTree<T>::loopInRange(void (*f)(void *), int start_n, int stop_n)
{
	if (start_n < 0 || start_n >= (int)heap.size() || start_n > stop_n
			|| stop_n >= (int)heap.size()) {
		return 1;
	}

	for (int x = start_n; x <= stop_n; x++)
		(*f)(&(heap[x]));

	return 0;
}

#endif
