#ifndef quicksort_h
#define quicksort_h

template <typename T>
int partition(T arr[], int low, int high)
{
	T pivot = arr[high];
	int i = low - 1;

	for (int j = low; j < high; j++)
	{
		if (!(arr[j] < pivot))
			continue;
		i++;
		std::swap(arr[i], arr[j]);
	}
	std::swap(arr[i + 1], arr[high]);

	return i + 1;
}

/*
 * usage quicksort(arr, 0, arrsize - 1)
 */
template <typename T>
void quicksort(T arr[], int low, int high)
{
	int part;

	if (low < high)
	{
		part = partition(arr, low, high);
		quicksort(arr, low, part - 1);
		quicksort(arr, part + 1, high);
	}
}

#endif
