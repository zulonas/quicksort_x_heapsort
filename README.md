# Quicksort & heapsort implementation (C++)

## Resources
### Linked list
- https://github.com/primeare/LinkedList
- https://github.com/spacehuhn/SimpleList
- https://github.com/TheBigMike00/CSC300_LinkedListC-


### Quick sort
- https://www.geeksforgeeks.org/quicksort-for-linked-list/


### Binary tree
- general information about binary tree -  http://cslibrary.stanford.edu/110/BinaryTrees.html
- binary search tree visualization - https://www.cs.usfca.edu/~galles/visualization/BST.html 
- https://gist.github.com/toboqus/def6a6915e4abd66e922


### Heap sort
- https://gist.github.com/santa4nt/030cf333808e0ef2fbe1
- https://opendsa-server.cs.vt.edu/ODSA/Books/CS3/html/Heapsort.html

### Binnary heap
- python implementation https://runestone.academy/runestone/books/published/pythonds/Trees/BinaryHeapImplementation.html
- visualization https://visualgo.net/en/heap
