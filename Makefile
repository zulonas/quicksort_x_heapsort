CC = g++
CFLAGS = -Wall -O

all: clean main

main:  $(objects) main.cpp
	$(CC) $(CFLAGS) $^ -o $@

memcheck: main
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -s ./$<

clean:
	rm -f core .*.sw* main
