#ifndef car_h
#define car_h

#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <random>
#include <string>

class Car
{
	public:
		std::string number;
		int year;
		Car();
		Car(const char * numberx, int year);
		friend bool operator> (const Car &a, const Car &b);
		friend bool operator< (const Car &a, const Car &b);
		friend bool operator== (const Car &a, const Car &b);
		friend bool operator!= (const Car &a, const Car &b);
		friend std::ostream& operator<<(std::ostream&, const Car&);
};

/*
 * Fill car object with random data using c++ <random> header(c++11)
 * via Mersenne Twister 19937 generator
 */
Car::Car()
{
	std::mt19937 gen(std::random_device{}());
	std::uniform_int_distribution<int> carYear(1950, 2020);
	std::uniform_int_distribution<int> carNumberText(65, 90); /* A-Z */
	std::uniform_int_distribution<int> carNumberNumber(48, 57); /* 0-9 */

	char carNumber[6];
	for (int x = 0; x < 3; x++)
		carNumber[x] = carNumberText(gen);
	for (int x = 3; x < 6; x++)
		carNumber[x] = carNumberNumber(gen);

	number = carNumber;
	year = carYear(gen);
}

Car::Car(const char *numberx, int year)
{
	number = numberx;
	this->year = year;
}

bool operator> (const Car &a, const Car &b)
{
	int temp = (a.number).compare(b.number);

	if (temp > 0 || (temp == 0 && a.year > b.year))
		return true;

	return false;
}

bool operator< (const Car &a, const Car &b)
{
	int temp = (a.number).compare(b.number);

	if (temp < 0 || (temp == 0 && a.year < b.year))
		return true;

	return false;
}

bool operator!= (const Car &a, const Car &b)
{
	return a.number.compare(b.number) != 0 || a.year != b.year;
}

bool operator== (const Car &a, const Car &b)
{
	return a.number.compare(b.number) == 0 && a.year == b.year;
}

// toString alike << operator overwrite
std::ostream& operator<<(std::ostream &strm, const Car &obj)
{
	return strm << obj.number << "\t" << obj.year;
}

#endif
